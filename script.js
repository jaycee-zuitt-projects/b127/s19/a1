const movies = [
	{
		title:'Excorcist',
		genre: 'horror',
		dateReleased: '1972',
		rating: 5,
		displayRating:function(){
			console.log('The movie ' + this.title + ' has' + this.rating + 'stars')
		},

	},
	{
		title:'Excorcist 2',
		genre: 'horror',
		dateReleased: '1975',
		rating: 4,
		displayRating:function(){
			console.log('The movie ' + this.title + ' has' + this.rating + 'stars')
		}
	}
]

//Activity 1
let showAllMovies = () => {for(let i=0; i < movies.length; i++){
		console.log(movies[i].title + " " + movies[i].genre)
	}
}

//test
showAllMovies()

//Activity 2

movies.sort((x,y) => x.rating-y.rating);

const showTitles = num =>{
	if (num < movies[0].rating || num > movies[(movies.length-1)].rating) {
		console.log(`No movies with that rating.`)
	}else {
		console.log(`Movies with ${num} and above ratings:`);
		let list=1;
		for(i = 0; i < movies.length; i++){
			if(movies[i].rating >= num) {
				console.log(`${list} ${movies[i].title} ${movies[i].rating} stars`);
				list++;
			}
		}
	}
}


//test
showTitles(4); //excorcist 1 & 2
showTitles(5); //excorcist 1 & 2
showTitles(3); //No movies with this ratings
showTitles(10);//No movies with this ratings
showTitles(2);//No movies with this ratings
